rsyslog:
  pkg:
    - installed

/etc/rsyslog.conf:
  file.managed:
    - source: salt://worldofsalty/m-master-rsys.conf
    - user: root
    - group: root
    - mode: 777