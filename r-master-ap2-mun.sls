apache2:
  pkg:
    - installed

munin:
  pkg:
    - installed

/var/www/munin:
  file.directory:
    - user: munin
    - name: /var/www/munin
    - group: munin
    - mode: 777

/etc/munin/apache.conf:
  file.managed:
    - source: salt://worldofsalty/m-master-ap2.conf
    - user: root
    - group: root
    - mode: 777

/etc/apache2/conf-available/munin.conf:
  file.managed:
    - source: salt://worldofsalty/m-master-mun-available.conf
    - user: root
    - group: root
    - mode: 777

/etc/apache2/conf-enabled/munin.conf:
  file.managed:
    - source: salt://worldofsalty/m-master-mun-enabled.conf
    - user: root
    - group: root
    - mode: 777

/etc/munin/munin.conf:
  file.managed:
    - source: salt://worldofsalty/m-master-mun.conf
    - user: root
    - group: root
    - mode: 777
    - template: jinja
    - defaults:
      private_ip_node: "10.0.0.13"
