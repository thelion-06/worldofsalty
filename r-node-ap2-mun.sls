apache2:
  pkg:
    - installed
munin:
  pkg:
    - installed
/etc/munin/munin-node.conf:
  file.managed:
    - source: salt://worldofsalty/n-node-mun.conf
    - user: root
    - group: root
    - mode: 777
    - template: jinja
    - defaults:
      private_ip_master: "^10\\.0\\.0\\.13$"
